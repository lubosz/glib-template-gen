/*
 * foo
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef FOO_BAR_H_
#define FOO_BAR_H_

#include <glib-object.h>

G_BEGIN_DECLS

#define FOO_TYPE_BAR foo_bar_get_type()
G_DECLARE_FINAL_TYPE (FooBar, foo_bar,
                      FOO, BAR, GObject)

FooBar *foo_bar_new (void);

G_END_DECLS

#endif /* FOO_BAR_H_ */
