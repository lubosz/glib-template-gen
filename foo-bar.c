/*
 * foo
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "foo-bar.h"

struct _FooBar
{
  GObject parent;
};

G_DEFINE_TYPE (FooBar, foo_bar, G_TYPE_OBJECT)

static void
foo_bar_init (FooBar *self)
{
  (void) self;
}

FooBar *
foo_bar_new (void)
{
  return (FooBar*) g_object_new (FOO_TYPE_BAR, 0);
}

static void
_finalize (GObject *gobject)
{
  FooBar *self = FOO_BAR (gobject);
  (void) self;
  G_OBJECT_CLASS (foo_bar_parent_class)->finalize (gobject);
}

static void
foo_bar_class_init (FooBarClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}
